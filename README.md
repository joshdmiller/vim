# Josh David Miller's Vim Configuration

Documentation coming soon.

## Dependencies

Obviously, you need a fairly recent version of Vim. I use 7.4, but slightly older versions may work
too. But some of the plugins require additional packages to be installed on your system. These
include:

- jsonlint
- jshint
- git
- coffeelint

